-- GEOBI / MICROSTRATEGY


-- HACK_CORPORATE_KPIS

  CREATE OR REPLACE FORCE VIEW "MICROSTRATEGY"."HACK_CORPORATE_KPIS" ("DAY_DATE", "BRAND", "MARKET", "BOOKINGS", "GROSS_SALES", "MARKUP", "REVENUE_MARGIN", "TOTAL_MARKETING_COST", "NET_REVENUE_MARGIN", "MARGINAL_PROFIT") AS 
  select 
  a.day as day_date,
  a.brand,
  a.market,
  sum(a.bookings) as bookings,
  sum(a.gross_sales) as gross_sales,
  sum(a.markup) as markup,
  sum(a.revenue_margin) as revenue_margin,
  sum(a.distrib_cost) + sum(pre_total_mkt_costs) as total_marketing_cost,
  sum(a.revenue_margin) - (sum(a.distrib_cost) + sum(pre_total_mkt_costs)) as net_revenue_margin,
  sum(a.revenue_margin) - (sum(a.distrib_cost) + sum(pre_total_mkt_costs) + sum(marginal_cost)) as marginal_profit
from
(select 
 to_date(f.date_id,'yyyymmdd') as day, 
 f.brand_id as brand,
 f.market_id as market,
 sum(f.num_purchases) as bookings,
 sum(f.gross_sales_purch) as gross_sales,
 sum(f.markup_purch) as markup,
 sum(f.distribution_cost) as distrib_cost,
 (sum(f.markup_purch) 
  + sum(f.commisions_purch) 
  + sum(f.commissions_insurances_purch) 
  + sum(f.overcommissions_percent*f.gross_sales_purch) 
  + sum(f.gds_incentive_non_lc_purch) 
  + sum(f.gds_incentive_lt_purch) 
  + sum(f.advertising_revenue) 
  + sum(f.other_revenue)) as revenue_margin
from 
 geobi_usr.f_odigeo_revenue f
group by to_date(f.date_id,'yyyymmdd'), f.brand_id, f.market_id) a,
(select 
 to_date(m.date_id,'yyyymmdd') as day, 
 m.brand_id as brand,
 m.market_id as market,
 (sum(m.acq_costs_mm) 
  + sum(m.map_costs) 
  + sum(m.mkt_costs) 
  + sum(m.other_mkt_costs)  
  + sum(m.offline_mkt_costs)) as pre_total_mkt_costs
from 
 geobi_usr.f_odigeo_marketing_costs m
group by to_date(m.date_id,'yyyymmdd'), m.brand_id, m.market_id) b,
(select 
 to_date(g.date_id,'yyyymmdd') as day, 
 g.brand_id as brand,
 g.market_id as market,
(  sum(g.merchant_cost) + sum(g.fraud_cost) + sum(g.call_center_cost) + sum(g.gds_fees) + sum(g.other_var_costs_b)) as marginal_cost 
from 
 geobi_usr.f_odigeo_marginal_costs g
group by to_date(g.date_id,'yyyymmdd'), g.brand_id, g.market_id) c
where
   a.day = b.day and 
   a.brand = b.brand and 
   a.market = b.market (+)  and 
   a.day = c.day and 
   a.brand = c.brand and 
   a.market = c.market (+) and
   a.day >= to_date('20140401','yyyymmdd')
group by a.day, a.brand, a.market
;



-- HACK_DAILY_BOOKINGS

  CREATE OR REPLACE FORCE VIEW "MICROSTRATEGY"."HACK_DAILY_BOOKINGS" ("TIMESTAMP", "MINUTE", "TIME", "TYPE", "WEBSITE", "ORDERS", "MARKUP", "NFI_AMADEUS", "NFI_LOWCOST", "NHO_HOTELS") AS 
  SELECT
    to_date(TO_CHAR(SYSDATE,'YYYMMDD')
    ||TO_CHAR(TIMESTAMP,'HH24MISS'),'YYYMMDDHH24MISS') AS TIMESTAMP,
    to_date(TO_CHAR(SYSDATE,'YYYMMDD')
    ||TO_CHAR(     TIMESTAMP,'HH24MI'),'YYYMMDDHH24MI') AS MINUTE,
    SUBSTR(TO_CHAR(TIMESTAMP,'HH24:MI'),1,4)
    ||'0' AS TIME,
    CASE
      WHEN TIMESTAMP >= TRUNC(SYSDATE-1)
      THEN 'TODAY'
      WHEN TIMESTAMP >= TRUNC(SYSDATE-8)
      THEN 'LW'
      WHEN TIMESTAMP >= TRUNC(SYSDATE-366)
      THEN 'LY'
      ELSE 'TOTO'
    END AS TYPE,
    WEBSITE,
    COUNT(*) AS ORDERS,
    SUM(OTHER_EXPENSES) MARKUP,
    SUM(NFI_AMADEUS) NFI_AMADEUS,
    SUM(NFI_LOWCOST) NFI_LOWCOST,
    SUM(NHO_HOTELS) NHO_HOTELS
  FROM
    ED_BOOKING@EDREALSTBY_READONLY BOOK
  WHERE ( BOOK.TIMESTAMP>=TRUNC(SYSDATE-1)
  AND BOOK.TIMESTAMP    <=(SYSDATE     -1)
  OR BOOK.TIMESTAMP     >=TRUNC(SYSDATE-1-7)
  AND BOOK.TIMESTAMP     <TRUNC(SYSDATE-1-6)
  OR BOOK.TIMESTAMP     >=TRUNC(SYSDATE-1-364)
  AND BOOK.TIMESTAMP     <TRUNC(SYSDATE-1-363) )
  GROUP BY
    to_date(TO_CHAR(SYSDATE,'YYYMMDD')
    ||TO_CHAR(TIMESTAMP,'HH24MISS'),'YYYMMDDHH24MISS'),
    to_date(TO_CHAR(SYSDATE,'YYYMMDD')
    ||TO_CHAR(     TIMESTAMP,'HH24MI'),'YYYMMDDHH24MI') ,
    SUBSTR(TO_CHAR(TIMESTAMP,'HH24:MI'),1,4)
    ||'0',
    CASE
      WHEN TIMESTAMP >= TRUNC(SYSDATE-1)
      THEN 'TODAY'
      WHEN TIMESTAMP >= TRUNC(SYSDATE-8)
      THEN 'LW'
      WHEN TIMESTAMP >= TRUNC(SYSDATE-366)
      THEN 'LY'
      ELSE 'TOTO'
    END,
    WEBSITE
 ;



-- HACK_PREPURCHASE_VW

  CREATE OR REPLACE FORCE VIEW "MICROSTRATEGY"."HACK_PREPURCHASE_VW" ("DAY_DATE", "BRAND", "MARKET", "MKT_CHANNEL_ID", "SEARCHES", "PURCHASES", "REVENUE_MARGIN_PURCH", "TOTAL_MKT_COSTS", "NET_REVENUE_PURCH", "VISITS", "ISSUED", "ORDERS") AS 
  SELECT
    day_date AS day_date,
    brand AS brand,
    market AS market,
    mkt_channel_id,
    SUM(searches) AS searches,
    SUM(purchases) AS purchases,
    SUM(revenue_margin_purch) AS revenue_margin_purch,
    SUM(total_mkt_costs) AS total_mkt_costs,
    SUM(net_revenue_purch) AS net_revenue_purch,
    SUM(visits) AS visits,
    SUM(issued) as issued,
    SUM(orders) AS orders
  FROM
    (SELECT
      to_date(date_id,'yyyymmdd') AS day_date,
      brand_id AS brand,
      market_id AS market,
      mkt_channel_id AS mkt_channel_id,
      SUM(searches) AS searches,
      0 AS purchases,
      SUM(revenue_margin_purch) AS revenue_margin_purch,
      SUM(costs) AS total_mkt_costs,
      (SUM(revenue_margin_purch) - SUM(costs)) AS net_revenue_purch,
      SUM(visits) AS visits,
      0 as issued,
      0 AS orders
    FROM
      GEOBI_USR.f_odigeo_mkt_channel_kpis aa
    WHERE
      date_id>=TO_CHAR(sysdate-2,'YYYYMMDD')
    GROUP BY
      date_id,
      brand_id,
      market_id,
      mkt_channel_id
    UNION ALL
    SELECT
      to_date(request_date,'yyyymmdd') AS day_date,
      brand AS brand,
      market AS market,
      TO_NUMBER(mkt_channel) AS mkt_channel_id,
      0 AS searches,
      COUNT(DISTINCT CASE WHEN BOOKING_STATUS_1 NOT IN ('HOL','INI') AND GOES_AGG = 1 THEN BOOKING_ID ELSE NULL END) AS purchases,
      0 AS revenue_margin_purch,
      0 AS total_mkt_costs,
      0 AS net_revenue_purch,
      0 AS visits,
      COUNT(DISTINCT CASE WHEN BOOKING_STATUS_1 IN ('ISS','CON') AND GOES_AGG = 1 THEN BOOKING_ID ELSE NULL END) as issued,
      COUNT(DISTINCT BOOKING_ID) AS orders
    FROM
      ODIGEO_SERVICES.HST_SERVICES_DETAIL_RAW
    WHERE
      REQUEST_DATE >= TO_CHAR(sysdate-2,'YYYYMMDD')
    GROUP BY
      to_date(request_date,'yyyymmdd'),
      brand,
      market,
      mkt_channel
    ) aa
  GROUP BY
    day_date,
    brand,
    market,
    mkt_channel_id
  ;
